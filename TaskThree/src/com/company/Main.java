package com.company;

import java.util.Scanner;

    public class Main {

        public static void main(String[] args) {
            boolean correctInput = true;

            System.out.print("Please enter the year and month: ");
            Scanner input = new Scanner(System.in);
            int[] stringNumbers = new int[2];
            for (int i = 0; i < 2; i++) {
                stringNumbers[i] = input.nextInt();
                if (stringNumbers[i] < 0) {
                    System.out.println("Incorrect input: Values cannot be negative. Please, try again!");
                    correctInput = false;
                }


            }
            int year = stringNumbers[0];
            int month = stringNumbers[1];

            if (month < 1 || month > 12) {
                System.out.println("Month should be in range of 1 and 12. Please, try again!");
                correctInput =false;
            }

            if (correctInput){
                System.out.print(year + " ");
                getMonth(month);
                System.out.print(" - ");
                getYearType(year);
            }

        }

        private static void getYearType(int year) {
            if (year % 4 == 0) {
                System.out.println("leap year");
            } else if ((year % 100 == 0) && (year % 400 == 0)) {
                System.out.println("leap year");
            } else {
                System.out.println("not leap year");
            }
        }

        private static void getMonth(int month) {

            String monthName;
            switch (month) {
                case 1:  monthName = "January";
                    break;
                case 2:  monthName = "February";
                    break;
                case 3:  monthName = "March";
                    break;
                case 4:  monthName = "April";
                    break;
                case 5:  monthName = "May";
                    break;
                case 6:  monthName = "June";
                    break;
                case 7:  monthName = "July";
                    break;
                case 8:  monthName = "August";
                    break;
                case 9:  monthName = "September";
                    break;
                case 10: monthName = "October";
                    break;
                case 11: monthName = "November";
                    break;
                case 12: monthName = "December";
                    break;
                default: monthName = "Invalid month";
                    break;
            }
            System.out.print(monthName);
        }
}
